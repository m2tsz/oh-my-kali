FROM kalilinux/kali-linux-docker

ENV TERM=xterm-256color
ENV LANGUAGE=en_US.UTF-8
ENV LC_ALL=en_US.UTF-8
ENV LANG=en_US.UTF-8
ENV LC_TYPE=en_US.UTF_8
ENV LC_CTYPE=en_US.UTF-8
ENV TZ=Europe/Warsaw

RUN apt update && apt install -y curl git zsh locales locales-all
RUN locale-gen && dpkg-reconfigure locales

RUN sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
RUN git clone https://github.com/romkatv/powerlevel10k.git /root/.oh-my-zsh/themes/powerlevel10k
RUN sed -i 's%ZSH_THEME="robbyrussell"%ZSH_THEME="powerlevel10k/powerlevel10k"%g' /root/.zshrc
RUN echo POWERLEVEL9K_MODE="awesome-patched" >> /root/.zshrc
 
CMD [ "zsh" ]
